﻿using Codebrudi.Logging.ColoredConsoleLogging;

ILogger<Program> logger = new Logger<Program>();

logger.LogTrace("Hello, World!");
logger.LogInformation("Hello, World!");
logger.LogWarning("Hello, World!");
logger.LogWarning("Hello, World!", new Exception("EXCEPTION"));
logger.LogError("Hello, World!");
logger.LogError("Hello, World!", new Exception("EXCEPTION"));
