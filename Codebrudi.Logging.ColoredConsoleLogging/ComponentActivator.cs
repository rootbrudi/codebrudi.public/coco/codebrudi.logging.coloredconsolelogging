using Codebrudi.CoCo.Contract.Bootstrapping;
using Microsoft.Extensions.DependencyInjection;

namespace Codebrudi.Logging.ColoredConsoleLogging;

public class ComponentActivator : IComponentActivator
{
    public void Activating() { }

    public void Activated() { }

    public void Deactivating() { }

    public void Deactivated() { }

    public void RegisterMappings(IServiceCollection services)
    {
        services.AddTransient(typeof(ILogger<>), typeof(Logger<>));
    }
}