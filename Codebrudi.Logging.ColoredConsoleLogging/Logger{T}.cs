﻿namespace Codebrudi.Logging.ColoredConsoleLogging;

public class Logger<T> : ILogger<T>
{
    private readonly ConsoleColor _backgroundColor;
    private readonly ConsoleColor _foregroundColor;

    public Logger()
    {
        _backgroundColor = Console.BackgroundColor;
        _foregroundColor = Console.ForegroundColor;
    }

    public void LogTrace(string message)
    {
        SetLoggingConsoleColor();
        Console.Write("||| ");
        Console.Write("TRACE");
        Console.Write(" || ");
        Console.Write(typeof(T).FullName);
        Console.Write(" || ");
        Console.Write(message);
        Console.WriteLine(" |||");
        ResetConsoleColor();
    }

    public void LogInformation(string message)
    {
        SetLoggingConsoleColor();
        Console.Write("||| ");
        SetConsoleForegroundColor(ConsoleColor.Green);
        Console.Write("INFO");
        SetConsoleForegroundColor(ConsoleColor.White);
        Console.Write("  || ");
        Console.Write(typeof(T).FullName);
        Console.Write(" || ");
        Console.Write(message);
        Console.WriteLine(" |||");
        ResetConsoleColor();
    }

    public void LogWarning(string message)
    {
        LogWarning(message, null);
    }

    public void LogWarning(string message, Exception? exception)
    {
        SetLoggingConsoleColor();
        Console.Write("|");
        SetConsoleForegroundColor(ConsoleColor.Yellow);
        Console.Write("|| ");
        Console.Write("WARN");
        SetConsoleForegroundColor(ConsoleColor.White);
        Console.Write("  || ");
        SetConsoleForegroundColor(ConsoleColor.Yellow);
        Console.Write(typeof(T).FullName);
        SetConsoleForegroundColor(ConsoleColor.White);
        Console.Write(" || ");
        SetConsoleForegroundColor(ConsoleColor.Yellow);
        Console.Write(message);
        if (exception is not null)
        {
            SetConsoleForegroundColor(ConsoleColor.White);
            Console.Write(" || ");
            SetConsoleForegroundColor(ConsoleColor.Red);
            Console.Write(exception.Message);
            SetConsoleForegroundColor(ConsoleColor.White);
            Console.Write(" || ");
            SetConsoleForegroundColor(ConsoleColor.Red);
            Console.Write(exception.StackTrace);
            SetConsoleForegroundColor(ConsoleColor.Yellow);
        }
        SetConsoleForegroundColor(ConsoleColor.White);
        Console.Write(" |");
        SetConsoleForegroundColor(ConsoleColor.Yellow);
        Console.WriteLine("||");
    }

    public void LogError(string message)
    {
        LogError(message, null);
    }

    public void LogError(string message, Exception? exception)
    {
        SetLoggingConsoleColor();
        Console.Write("|");
        SetConsoleForegroundColor(ConsoleColor.Red);
        Console.Write("|| ");
        Console.Write("ERROR");
        SetConsoleForegroundColor(ConsoleColor.White);
        Console.Write(" || ");
        SetConsoleForegroundColor(ConsoleColor.Red);
        Console.Write(typeof(T).FullName);
        SetConsoleForegroundColor(ConsoleColor.White);
        Console.Write(" || ");
        SetConsoleForegroundColor(ConsoleColor.Red);
        Console.Write(message);
        if (exception is not null)
        {
            SetConsoleForegroundColor(ConsoleColor.White);
            Console.Write(" || ");
            SetConsoleForegroundColor(ConsoleColor.Red);
            Console.Write(exception.Message);
            SetConsoleForegroundColor(ConsoleColor.White);
            Console.Write(" || ");
            SetConsoleForegroundColor(ConsoleColor.Red);
            Console.Write(exception.StackTrace);
        }
        SetConsoleForegroundColor(ConsoleColor.White);
        Console.Write(" |");
        SetConsoleForegroundColor(ConsoleColor.Red);
        Console.WriteLine("||");
        ResetConsoleColor();
    }

    private void SetLoggingConsoleColor() => SetConsoleColors(ConsoleColor.Black, ConsoleColor.White);
    private void ResetConsoleColor() => SetConsoleColors(_backgroundColor, _foregroundColor);
    private void SetConsoleColors(ConsoleColor backgroundColor, ConsoleColor foregroundColor)
    {
        SetConsoleBackgroundColor(backgroundColor);
        SetConsoleForegroundColor(foregroundColor);
    }

    private void SetConsoleBackgroundColor(ConsoleColor backgroundColor)
    {
        Console.BackgroundColor = backgroundColor;
    }

    private void SetConsoleForegroundColor(ConsoleColor foregroundColor)
    {
        Console.ForegroundColor = foregroundColor;
    }
}
